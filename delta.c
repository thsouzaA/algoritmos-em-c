#include <stdio.h>
#include <locale.h>
#include <math.h>

int main () 
{
	//  algoritmo que calcula o valor de delta de uma fun��o quadr�tica  //*
	setlocale(LC_ALL, "Portuguese");
	int a, b, c;
	printf("digite o valor de a da fun��o: \n");
	scanf("%d", &a);
	printf("digite o valor de b da fun��o: \n");
	scanf("%d", &b);
	printf("digite o valor de c da fun��o: \n");
	scanf("%d", &c);
		int delta = (b)*(b)-4*(a*c);
	printf("valor de delta /_\\ = %d \n", delta);
	printf("\n");
	if ( delta > 0 ) 
	{
		printf("valor de delta > 0, logo, a equa��o %dx� %dx %d ter� duas raizes reais e distintas! \n", a, b, c);
	}
	if ( delta < 0 ) 
	{
		printf("valor de delta < 0, logo, a equa��o %dx� %dx %d n�o ter� raizes reais.", a, b, c);
	}
	if ( delta == 0 ) 
	{
		printf("valor de delta = 0, logo, a equa��o %dx� %dx %d ter� duas raizes reais e iguais!", a, b, c);
	}
}
